plugins {
    id(Plugins.kapt)
}

tasks {
    compileKotlin {
        kotlinOptions {
            jvmTarget = JVM.target
            freeCompilerArgs = listOf(
                CompilerArguments.inlineClasses,
                CompilerArguments.coroutines,
                CompilerArguments.time
            )
        }
    }
}
