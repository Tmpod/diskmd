import dev.tmpod.diskmd.startBot

suspend fun main(args: Array<String>) {
    bot(token = "TOKEN", commandPrefix = "!") {
        command("ping", "p") {
            brief = "Pings the bot"
            description = """
                          Calculates the raw Gateway latency as well as the message roundtrip latency.
                          """.trimIndent()

            logic {
                var msg: Message? = null
                // This will measure the time it takes to send and receive a message
                val roundtripLatency = measureTimeMillis {
                    msg = send("> ***Pinging...***")
                }

                // Now we send a nice message
                send(
                        """
                        >>> **Pong!** :ping_pong:
                        **Gateway:** `${kord.gateway.latency}`ms
                        **Roundtrip:** `${roundtripLatency}`ms
                        """.trimIndent()
                )
            }
        }
    }
}
