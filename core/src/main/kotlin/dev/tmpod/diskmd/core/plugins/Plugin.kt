/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core.plugins

import com.gitlab.kordlib.core.event.Event
import dev.tmpod.diskmd.core.DiskmdDsl
import dev.tmpod.diskmd.core.commands.*
import dev.tmpod.diskmd.core.listeners.Listener
import dev.tmpod.diskmd.core.listeners.ListenerLogic

typealias PluginSetupTeardownLogic = suspend () -> Unit

/**
 *
 */
data class Plugin(
    val name: String,
    val commands: Set<Command> = setOf(),
    val checks: Set<CommandCheck> = setOf(),
    val listeners: Set<Listener<Event>> = setOf(),
    val setup: PluginSetupTeardownLogic? = null,
    val teardown: PluginSetupTeardownLogic? = null,
)

/**
 *
 */
@DiskmdDsl
class PluginBuilder(val name: String) {
    /**
     *
     */
    var description: String? = null


    /**
     *
     */
    val commands: MutableSet<BaseCommand> = mutableSetOf()

    fun command(vararg names: String, builder: CommandBuilder.() -> Unit) {
        commands.add(dev.tmpod.diskmd.core.commands.command(*names, builder=builder))
    }

    fun group(vararg names: String, builder: CommandGroupBuilder.() -> Unit) {
        commands.add(dev.tmpod.diskmd.core.commands.group(*names, builder=builder))
    }


    /**
     *
     */
    val checks: MutableList<CommandCheck> = mutableListOf()

    fun check(name: String? = null, logic: CommandCheckLogic) {
        checks.add(CommandCheck(name, logic))
    }


    /**
     *
     */
    val listeners: MutableList<Listener<Event>> = mutableListOf()

    inline fun <reified E : Event> listener(name: String? = null, noinline logic: ListenerLogic<E>) {
        @Suppress("UNCHECKED_CAST")
        listeners.add(dev.tmpod.diskmd.core.listeners.listener(name, logic) as Listener<Event>)
    }


    /**
     *
     */
    var setup: PluginSetupTeardownLogic? = null

    /**
     *
     */
    fun setup(logic: PluginSetupTeardownLogic) {
        setup = logic
    }

    /**
     *
     */
    var teardown: PluginSetupTeardownLogic? = null

    /**
     *
     */
    fun teardown(logic: PluginSetupTeardownLogic) {
        teardown = logic
    }


    /**
     *
     */
    fun build(): Plugin {

    }
}

/**
 *
 */
inline fun plugin(name: String, builder: PluginBuilder.() -> Unit) = PluginBuilder(name).apply(builder).build()
