/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core.commands

import dev.tmpod.diskmd.core.commands.args.CommandArgs
import dev.tmpod.diskmd.core.utils.makeSignature
import kotlinx.serialization.serializer

/**
 *
 */
data class Command(
    override val names: List<String>,
    override val brief: String?,
    override val description: String?,
    override val usageExample: String?,
    override val parent: CommandGroup?,
    override val inheritParentChecks: Boolean = true,
    override val signature: String,
    override val checks: List<CommandCheck>,
    override val processArgs: ProcessArgs<Any>,
    override val logic: CommandLogic<Any>,
) : BaseCommand


/**
 *
 */
class CommandBuilder {
    /**
     *
     */
    var names: List<String> = listOf()
    /**
     *
     */
    var brief: String? = null
    /**
     *
     */
    var description: String? = null
    /**
     *
     */
    var usageExample: String? = null


    /**
     *
     */
    var signature: String? = null
    /**
     *
     */
    var processArgs: ProcessArgs<Any>? = null
    /**
     *
     */
    var logic: CommandLogic<Any>? = null


    /**
     *
     */
    val checks: MutableList<CommandCheck> = mutableListOf()

    /**
     *
     */
    fun check(identifier: String? = null, logic: CommandCheckLogic) {
        checks.add(CommandCheck(identifier, logic))
    }


    /**
     *
     */
    @Suppress("UNCHECKED_CAST")
    inline fun <reified Args : Any> logic(noinline logic: CommandLogic<Args>) {
        this.logic = logic as CommandLogic<Any>
        processArgs = { CommandArgs.parse(serializer<Args>(), it) }
        signature = makeSignature(serializer<Args>().descriptor)
    }


    /**
     *
     */
    fun build(parent: CommandGroup? = null, inheritParentChecks: Boolean = true): Command {
        require(names.isNotEmpty()) { "At least one name must be provided!" }
        require(logic != null && processArgs != null && signature != null) { "You must provide a logic for the command!" }

        return Command(
            names = names,
            brief = brief,
            description = description,
            usageExample = usageExample,
            parent = parent,
            inheritParentChecks = inheritParentChecks,
            signature = signature!!,  // eeeh
            processArgs = processArgs!!,
            logic = logic!!,
            checks = checks
        )
    }
}


/**
 *
 */
inline fun command(vararg names: String, builder: CommandBuilder.() -> Unit) =
    CommandBuilder().apply { this.names = names.toList() }.apply(builder).build()
