/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core.utils

// Citation: https://en.wikipedia.org/wiki/Quotation_mark
val DEFAULT_QUOTE_MAPPING = mapOf(
//        "```" to "```",
//        "``" to "''",
//        "‛‛" to "’’",
    '`' to '`',
    '‛' to '’',
    '\'' to '\'',
    '"' to '"',
    '“' to '”',
    '‘' to '’',
    '«' to '»',
    '‹' to '›',
    '《' to '》',
    '〈' to '〉',
    '「' to '」',
    '｢' to '｣',
    '『' to '』',
    '〝' to '〞',
    '＂' to '＂',
    '＇' to '＇'
)
const val DEFAULT_ESCAPE_CHAR = '\\'
const val DEFAULT_SEPARATOR_CHAR = ' '

/**
 * Splits a string into a list of bits, taking quotes and escaping into account.
 * A maximum amount of bits can be requested through the [amount] parameter.
 * The escape and separator characters can also be changed, as well as the valid quote pairs.
 *
 * The defaults for the quotes, escape and separator chars can be found on the [DEFAULT_QUOTE_MAPPING],
 * [DEFAULT_ESCAPE_CHAR], [DEFAULT_SEPARATOR_CHAR] constants, respectively.
 */
fun splitByQuotes(
    string: String,
    amount: Int? = null,  // Amount of bits to get
    escapeChar: Char = DEFAULT_ESCAPE_CHAR,
    separatorChar: Char = DEFAULT_SEPARATOR_CHAR,
    quotes: Map<Char, Char> = DEFAULT_QUOTE_MAPPING
): List<String> {
    var currentCloseQuote: Char? = null
    var lastCharWasEscape: Boolean = false
    val parsedArgs = mutableListOf<String>()
    val buffer = StringBuilder()

    for (c in string) {
        if (lastCharWasEscape) {
            lastCharWasEscape = false
            buffer.append(c)
            continue
        }

        if (c == escapeChar) {
            lastCharWasEscape = true
            continue
        }

        if (currentCloseQuote == null) {
            currentCloseQuote = quotes[c]

            if (currentCloseQuote != null) {
                continue
            }

            // Maybe consider using Char#isWhitespace ?
            if (c == separatorChar) {
                // If we get several spaces in a row, don't treat them as lots of empty string arguments.
                if (buffer.isNotEmpty()) {
                    parsedArgs += buffer.toString()
                    buffer.clear()
                }
            } else {
                buffer.append(c)
            }

        } else if (c == currentCloseQuote) {
            parsedArgs += buffer.toString()
            buffer.clear()
            currentCloseQuote = null
        } else {
            buffer.append(c)
        }

        // Stop parsing if we got the amount we need
        if (parsedArgs.size == amount) {
            break
        }
    }

    // If we hit end of string, this is likely filled, so flush once more if so.
    if (buffer.isNotEmpty()) {
        parsedArgs += buffer.toString()
    }

    require(currentCloseQuote == null) { "Expected a closing $currentCloseQuote" }

    return parsedArgs.toList()
}


/**
 *
 */
fun String.removePrefix(prefix: CharSequence, ignoreCase: Boolean = false): String {
    if (startsWith(prefix, ignoreCase)) {
        return substring(prefix.length)
    }
    return this
}

/**
 * Attempts to remove one of the given [CharSequence] from the beginning of a [String], returning a pair of the removed
 * prefix and new string. If no prefix is removed, meaning the string is left intact, the prefix will be null.
 *
 * You can optionally enable case insensitivity by passing [ignoreCase] as `true`.
 */
fun String.removePrefixes(prefixes: Collection<CharSequence>, ignoreCase: Boolean = false): Pair<CharSequence?, String> {
    prefixes.forEach {
        if (startsWith(it, ignoreCase)) {
            return it to substring(it.length)
        }
    }
    return null to this
}
