/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core.di

import org.koin.core.Koin
import org.koin.core.KoinApplication
import org.koin.core.context.KoinContext

/**
 *
 */
object DiskmdKoinContext : KoinContext {
    private var koinApp: KoinApplication? = null

    override fun get(): Koin = koinApp?.koin ?: throw error("Koin application hasn't been started!")

    override fun getOrNull(): Koin? = koinApp?.koin

    override fun setup(koinApplication: KoinApplication) {
        require(koinApp == null) { "A Koin application is already running! Close that one first." }
        koinApp = koinApplication
    }

    override fun stop() {
        koinApp?.close()
        koinApp = null
    }
}
