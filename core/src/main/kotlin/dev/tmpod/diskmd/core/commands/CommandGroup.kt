/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core.commands

/**
 *
 */
data class CommandGroup(
    override val names: List<String>,
    override val brief: String?,
    override val description: String?,
    override val usageExample: String?,
    override val parent: CommandGroup?,
    override val inheritParentChecks: Boolean = true,
    override val signature: String,
    override val checks: List<CommandCheck>,
    override val processArgs: ProcessArgs,
    override val logic: CommandLogic,
) : CommandRegistry(), BaseCommand
