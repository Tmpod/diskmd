/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core.commands

import dev.tmpod.diskmd.core.exceptions.CommandCheckFailureException

/**
 *
 */
typealias ProcessArgs<Args> = (String) -> Args
/**
 *
 */
typealias CommandLogic<Args> = suspend CommandContext.(Args) -> Unit


/**
 *
 */
interface BaseCommand {
    val names: List<String>
    val brief: String?
    val description: String?
    val usageExample: String?
    val parent: CommandGroup?
    val inheritParentChecks: Boolean
    val signature: String
    val checks: List<CommandCheck>
    val processArgs: ProcessArgs<Any>?
    val logic: CommandLogic<Any>

    /**
     *
     */
    val fullyQualifiedName: String
        get() = parent?.fullyQualifiedName?.plus(name) ?: name

    /**
     *
     */
    val name: String
        get() = names.first()


    /**
     *
     */
    suspend fun invoke(context: CommandContext) {
        logic(context, processArgs(context.rawArgs))
    }

    /**
     *
     */
    suspend fun runChecks(context: CommandContext) {
        if (parent != null && inheritParentChecks) {
            parent!!.runChecks(context)
        }

        checks.forEach { c ->
            // Short circuiting. If a custom exception is thrown inside the check logic we let it propagate.
            if (!c.logic(context)) {
                throw CommandCheckFailureException(
                    context,
                    c.name?.let { "$it check failed" } ?: "Check failed")
            }
        }
    }
}
