/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core.listeners

import com.gitlab.kordlib.core.event.Event
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filterIsInstance

typealias ListenerLogic<E> = suspend E.() -> Unit

/**
 *
 */
class Listener<E : Event>(
    val name: String? = null,
    val setup: (Flow<Event>) -> Flow<E>,
    val logic: ListenerLogic<E>
) {
    override fun toString(): String = """Listener(name="$name")"""
}

/**
 *
 */
inline fun <reified E : Event> listener(name: String? = null, noinline logic: ListenerLogic<E>): Listener<E> {
    return Listener(name, { it.filterIsInstance() }, logic)
}
