/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core.commands.args

import dev.tmpod.diskmd.core.utils.splitByQuotes
import kotlinx.serialization.*
import kotlinx.serialization.modules.EmptySerializersModule
import kotlinx.serialization.modules.SerializersModule

// Just some aliases for less typing and a more idiomatic API.
/**
 *
 */
typealias args = Serializable
/**
 *
 */
typealias Greedy<T> = GreedyArgsSerializer<T>


/**
 *
 */
@OptIn(ExperimentalSerializationApi::class)
class CommandArgs(override val serializersModule: SerializersModule = EmptySerializersModule) : StringFormat {
    companion object {
        val Default = CommandArgs()

        // Utility quick methods
        /**
         *
         */
        fun <T> parse(deserializer: DeserializationStrategy<T>, string: String): T {
            return Default.decodeFromString(deserializer, string)
        }

        /**
         *
         */
        fun <T> parse(deserializer: DeserializationStrategy<T>, bits: List<String>): T {
            return Default.decodeFromBits(deserializer, bits)
        }

        /**
         *
         */
        inline fun <reified T> parse(string: String): T {
            return parse(serializer(), string)
        }

        /**
         *
         */
        inline fun <reified T> parse(bits: List<String>): T {
            return parse(serializer(), bits)
        }
    }

    /**
     *
     */
    override fun <T> decodeFromString(deserializer: DeserializationStrategy<T>, string: String): T {
        return ArgumentDecoder(splitByQuotes(string)).decodeSerializableValue(deserializer)
    }

    /**
     *
     */
    fun <T> decodeFromBits(deserializer: DeserializationStrategy<T>, bits: List<String>): T {
        return ArgumentDecoder(bits).decodeSerializableValue(deserializer)
    }

    /**
     *
     */
    override fun <T> encodeToString(serializer: SerializationStrategy<T>, value: T): String {
        throw NotImplementedError("Encoding not supported!")
    }
}
