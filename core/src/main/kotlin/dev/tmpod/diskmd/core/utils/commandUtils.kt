/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core.utils

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.elementDescriptors

/**
 *
 */
@OptIn(ExperimentalSerializationApi::class)
fun makeSignature(descriptor: SerialDescriptor): String {
    return buildString {
        descriptor.elementDescriptors.forEachIndexed { i, d ->
            // We consider nullable values as optional parameters too.
            if (d.isNullable || descriptor.isElementOptional(i)) {
                append("[${d.serialName}]")
            } else {
                append("<${d.serialName}>")
            }
            append(' ')  // Will get an extra space at the end, but it's no big deal. Might change later.
        }
    }
}
