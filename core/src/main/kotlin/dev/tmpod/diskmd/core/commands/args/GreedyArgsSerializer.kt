/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core.commands.args

import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.encoding.CompositeDecoder

/**
 *
 */
class GreedyArgsSerializer<T>(private val serializer: KSerializer<T>) : KSerializer<List<T>> {
    /**
     *
     */
    override val descriptor: SerialDescriptor = serializer.descriptor

    /**
     *
     */
    override fun deserialize(decoder: Decoder): List<T> {
        val builder = mutableListOf<T>()
        val compositeDecoder = decoder.beginStructure(descriptor)
        var listIndex: Int = 0
        while (true) {
            val done = compositeDecoder.decodeElementIndex(descriptor)
            if (done == CompositeDecoder.DECODE_DONE) break
            try {
                builder.add(compositeDecoder.decodeSerializableElement(descriptor, listIndex++, serializer, null))
            } catch (ex: SerializationException) {
                break
            }
        }
        compositeDecoder.endStructure(descriptor)
        return builder
    }

    /**
     *
     */
    override fun serialize(encoder: Encoder, value: List<T>) {
        throw NotImplementedError("Serialization not supported!")
    }
}
