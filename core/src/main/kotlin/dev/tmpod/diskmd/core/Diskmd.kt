/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core

import com.gitlab.kordlib.core.Kord
import dev.tmpod.diskmd.core.commands.CommandRegistryConfig
import dev.tmpod.diskmd.core.commands.PrefixSupplyingLogic
import dev.tmpod.diskmd.core.commands.commandsModule
import dev.tmpod.diskmd.core.di.DiskmdKoinContext
import dev.tmpod.diskmd.core.listeners.listenersModule
import dev.tmpod.diskmd.core.plugins.pluginsModule
import org.koin.core.KoinApplication
import org.koin.core.context.startKoin
import org.koin.dsl.KoinAppDeclaration
import org.koin.dsl.koinApplication
import org.koin.dsl.module

typealias DiskmdAppDeclaration = DiskmdAppBuilder.() -> Unit

/**
 *
 */
@DiskmdDsl
class DiskmdAppBuilder {
    /**
     *
     */
    var appDescription: String? = null


    /**
     *
     */
    var defaultPrefixes: Set<String> = setOf()

    /**
     *
     */
    var prefixSupplier: PrefixSupplyingLogic? = null


    /**
     *
     */
    lateinit var botToken: String


    /**
     *
     */
    var koin: KoinAppDeclaration? = null

    /**
     * Exposes a Koin app declaration that will be applied after the default one.
     */
    fun koin(appDeclaration: KoinAppDeclaration) {
        koin = appDeclaration
    }


    /**
     * Runs some input verification logic to make sure the app declaration is valid.
     */
    fun verify() {
        require(defaultPrefixes.isNotEmpty() || prefixSupplier != null) {
            "You must provide a set of static prefixes, a custom prefix supplier or both!"
        }
    }
}

/**
 *
 */
suspend fun diskmdApp(appDeclaration: DiskmdAppDeclaration): KoinApplication {
    val declaration = DiskmdAppBuilder().apply(appDeclaration)
    declaration.verify()
    val config = DiskmdConfig(
        metadata = DiskmdMetadata(declaration.appDescription),
        kord = KordConfig(token = declaration.botToken),
        commandRegistry = CommandRegistryConfig(declaration.defaultPrefixes, declaration.prefixSupplier),
    )
    // Kord has to be created eagerly
    val kord = Kord("") { }
    val configAndKordModule = module {
        single { config }
        single { kord }
    }
    return koinApplication {
        modules(configAndKordModule, commandsModule, listenersModule, pluginsModule)
        // Apply the optional Koin app declaration
        declaration.koin?.invoke(this)
    }
}


/**
 *
 */
suspend fun startDiskmdApp(appDeclaration: DiskmdAppDeclaration) {
    startKoin(DiskmdKoinContext, diskmdApp(appDeclaration))
}
