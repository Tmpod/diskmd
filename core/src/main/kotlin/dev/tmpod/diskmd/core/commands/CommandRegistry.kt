/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core.commands

import com.gitlab.kordlib.core.Kord
import com.gitlab.kordlib.core.event.message.MessageCreateEvent
import dev.tmpod.diskmd.core.di.DiskmdComponent
import dev.tmpod.diskmd.core.exceptions.CommandNotFoundException
import dev.tmpod.diskmd.core.utils.removePrefixes
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import mu.KLogger
import mu.KotlinLogging
import org.koin.core.inject

/**
 *
 */
typealias PrefixSupplyingLogic = suspend MessageCreateEvent.() -> Set<String>

/**
 *
 */
data class CommandRegistryConfig(
    val defaultPrefixes: Set<String> = setOf(),
    val prefixSupplier: PrefixSupplyingLogic? = null
)


/**
 *
 */
open class CommandRegistry : DiskmdComponent() {
    private val config: CommandRegistryConfig by inject()
    private val kord: Kord by inject()

    private val logger: KLogger = KotlinLogging.logger { }
    private val commandsMap: MutableMap<String, BaseCommand> = mutableMapOf()
    private val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.Default)

    /**
     *
     */
    val commands: Set<BaseCommand> get() = commandsMap.values.toSet()

    /**
     *
     */
    fun walkAllCommands(): Sequence<BaseCommand> = sequence {
        commands.forEach { c ->
            when (c) {
                is CommandGroup -> c.walkAllCommands().forEach { yield(it) }
                else -> yield(c)
            }
        }
    }


    /**
     *
     */
    fun addCommand(command: BaseCommand) {
        require(command.names.intersect(commandsMap.keys).isEmpty()) {
            "One of the command's name is already registered!"
        }

        command.names.forEach {
            commandsMap[it] = command
            logger.debug { "Added command name $it" }
        }
    }

    /**
     *
     */
    fun removeCommand(command: BaseCommand) {
        command.names.forEach {
            commandsMap.remove(it, command)
            logger.debug { "Removed command name $it" }
        }
    }

    /**
     *
     */
    fun removeCommand(name: String) {
        removeCommand(getCommand(name) ?: throw NoSuchElementException("Cannot find command with name $name!"))
    }

    /**
     *
     */
    fun clearCommands() {
        commandsMap.clear()
        logger.info { "Cleared commands!" }
    }

    /**
     *
     */
    fun getCommand(name: String): BaseCommand? = commandsMap[name]


    /**
     *
     */
    operator fun contains(command: BaseCommand) = command in commands

    /**
     *
     */
    operator fun plusAssign(command: BaseCommand) {
        addCommand(command)
    }

    /**
     *
     */
    operator fun minusAssign(command: BaseCommand) {
        removeCommand(command)
    }


    /**
     *
     */
    private fun getCommandParent(): CommandGroup? = null


    /**
     *
     */
    suspend fun processCommands(event: MessageCreateEvent, partial: Pair<String, String>? = null) {
        // Processing from room, meaning we have to check the prefix
        val nextContent: String
        val actualPrefix: String

        if (partial == null) {
            // Unsure if this is a good way to do this
            val prefixes = config.prefixSupplier
                ?.let { it(event) union config.defaultPrefixes }
                ?: config.defaultPrefixes

            val (prefix, new) = event.message.content.trim().removePrefixes(prefixes, true)
            if (prefix == null) {
                return
            }

            nextContent = new
            actualPrefix = prefix as String  // :thonk:
        } else {
            nextContent = partial.first.trim()
            actualPrefix = partial.second
        }

        val nextWhitespace = nextContent.indexOfFirst(Char::isWhitespace)
        val commandCandidateName = nextContent.slice(0 until nextWhitespace)
        val rest = nextContent.substring(nextWhitespace)
        val commandCandidate = getCommand(commandCandidateName)
            ?: throw CommandNotFoundException(
                CommandContext(
                    event = event,
                    command = null,
                    prefix = actualPrefix,
                    rawArgs = rest
                ),
                commandCandidateName
            )

        when (commandCandidate) {
            is CommandGroup -> processCommands(event, rest to actualPrefix)
            else -> {
                val context = CommandContext(
                    event = event,
                    command = commandCandidate,
                    prefix = actualPrefix,
                    rawArgs = rest
                )
                commandCandidate.runChecks(context)
                coroutineScope.launch { commandCandidate.invoke(context) }
            }
        }
    }


    /**
     *
     */
    fun start() {
        logger.info { "Starting..." }
        kord.events
            .filterIsInstance<MessageCreateEvent>()
            .onEach { processCommands(it) }
            .catch { logger.error(it) { "Caught an unexpected exception!" } }
            .launchIn(coroutineScope)
        logger.info { "Started!" }
    }

    /**
     *
     */
    fun stop() {
        logger.info { "Stopping..." }
        coroutineScope.cancel("CommandRegistry is stopping!")
        logger.info { "Stopped!" }
    }
}
