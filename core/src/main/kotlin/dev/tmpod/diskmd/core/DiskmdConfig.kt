/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core

import dev.tmpod.diskmd.core.commands.CommandRegistryConfig

/**
 * Simple dataclass that holds some optional metadata, useful for information commands.
 */
data class DiskmdMetadata(val appDescription: String? = null)

/**
 *
 */
data class KordConfig(val token: String)

/**
 * A dataclass holding all required configuration parameters for `diskmd`.
 */
data class DiskmdConfig(val metadata: DiskmdMetadata, val kord: KordConfig, val commandRegistry: CommandRegistryConfig)
