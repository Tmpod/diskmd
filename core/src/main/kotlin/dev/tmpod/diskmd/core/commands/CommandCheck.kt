/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core.commands

import com.gitlab.kordlib.core.behavior.channel.GuildChannelBehavior
import dev.tmpod.diskmd.core.exceptions.CommandCheckFailureException

/**
 *
 */
typealias CommandCheckLogic = suspend CommandContext.() -> Boolean

/**
 *
 */
data class CommandCheck(
    val name: String? = null,
    val logic: CommandCheckLogic
) {
    /**
     *
     */
    suspend fun run(context: CommandContext) {
        val result = try {
            logic(context)
        } catch (ex: Exception) {
            throw CommandCheckFailureException(context, ex)
        }

        if (!result) {
            throw CommandCheckFailureException(context)
        }
    }

    companion object Common {
        /**
         *
         */
        val guildOnly = CommandCheck("guildOnly") { channel is GuildChannelBehavior }
        /**
         *
         */
        val dmOnly = CommandCheck("dmOnly") { channel !is GuildChannelBehavior }
    }
}
