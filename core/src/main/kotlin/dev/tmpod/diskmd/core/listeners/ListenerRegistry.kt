/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core.listeners

import com.gitlab.kordlib.core.Kord
import com.gitlab.kordlib.core.event.Event
import dev.tmpod.diskmd.core.di.DiskmdComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import mu.KLogger
import mu.KotlinLogging
import org.koin.core.inject

/**
 *
 */
open class ListenerRegistry : DiskmdComponent() {
    private val kord: Kord by inject()

    private val logger: KLogger = KotlinLogging.logger { }
    private val _listeners: MutableMap<Listener<Event>, Job?> = mutableMapOf()
    // is good to use the default dispatchers?
    private val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.Default)
    private var started: Boolean = false

    /**
     *
     */
    val listeners: Set<Listener<Event>> get() = _listeners.keys

    /**
     *
     */
    fun addListener(listener: Listener<Event>) {
        // A bit inefficient on the name check. Could have a separate listeners name set for quicker lookup.
        require(listener !in _listeners && listener.name !in _listeners.keys.map(Listener<Event>::name).toSet()) {
            "There's either a listener with that name already registered or that listener is already registered!"
        }

        if (started) {
            setupListener(listener)
        } else {
            _listeners[listener] = null
        }
    }

    /**
     *
     */
    fun removeListener(name: String) {
        _listeners.remove(_listeners.keys.first { it.name == name })
    }

    /**
     *
     */
    fun removeListener(listener: Listener<Event>) {
        _listeners.remove(listener)
    }

    /**
     *
     */
    fun getListener() {
    }

    private fun setupListener(listener: Listener<Event>) {
        _listeners[listener] = listener.setup(kord.events)
            .catch { e -> logger.error(e) { "An unexpected exception occurred on listener ${listener.name ?: "unnamed"}!" } }
            .launchIn(coroutineScope)
    }

    /**
     *
     */
    fun start() {
        logger.info { "Starting..." }
        require(!started) { "ListenerRegistry is already started!" }
        _listeners.keys.forEach(::setupListener)
        started = true
        logger.info { "Started!" }
    }

    /**
     *
     */
    fun stop() {
        logger.info { "Stopping..." }
        coroutineScope.cancel("ListenerRegistry is stopping!")
        _listeners.forEach { _listeners[it.key] = null }
        started = false
        logger.info { "Stopped!" }
    }
}
