/*
 *   This file is part of disKmd, a command framework built on top of Kord.
 *   Copyright (C) 2020  Tmpod
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.diskmd.core.commands.args

import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.AbstractDecoder
import kotlinx.serialization.encoding.CompositeDecoder
import kotlinx.serialization.internal.AbstractCollectionSerializer


/**
 *
 */
@OptIn(ExperimentalSerializationApi::class)
open class ArgumentDecoder(
    private val bits: List<String>,
) : AbstractDecoder() {
    internal var index: Int = 0

    internal fun peekBit(): String = bits[index]
    internal fun nextBit(): String = bits[index++]
    internal fun peekNextBit(): String = bits[index + 1]

    private fun fail(message: String): Nothing {
        --index
        throw SerializationException(message)
    }

    override fun decodeSequentially(): Boolean = true

    override fun decodeBoolean(): Boolean {
        return when (val bit = nextBit().toLowerCase()) {
            in "true", "on", "yes" -> true
            in "false", "off", "no" -> true
            else -> fail("Expected boolean value but got $bit instead!")
        }
    }

    override fun decodeChar(): Char {
        return nextBit().singleOrNull()
            ?: fail("Expected char but got more than one character!")
    }

    override fun decodeString(): String {
        return nextBit()
    }

    override fun decodeByte(): Byte {
        val bit = nextBit()
        return bit.toByteOrNull() ?: fail("Expected byte value but got $bit instead!")
    }

    override fun decodeShort(): Short {
        val bit = nextBit()
        return bit.toShortOrNull() ?: fail("Expected short value but got $bit instead!")
    }

    override fun decodeInt(): Int {
        val bit = nextBit()
        return bit.toIntOrNull() ?: fail("Expected int value but got $bit instead!")
    }

    override fun decodeLong(): Long {
        val bit = nextBit()
        return bit.toLongOrNull() ?: fail("Expected long value but got $bit instead!")
    }

    override fun decodeFloat(): Float {
        val bit = nextBit()
        return bit.toFloatOrNull() ?: fail("Expected float value but got $bit instead!")
    }

    override fun decodeDouble(): Double {
        val bit = nextBit()
        return bit.toDoubleOrNull() ?: fail("Expected double value but got $bit instead!")
    }

    override fun decodeEnum(enumDescriptor: SerialDescriptor): Int {
        val bit = nextBit()
        // This adds case insensitivity, since SerialDescriptor#getElementIndex doesn't provide that.
        val name = enumDescriptor.elementNames.firstOrNull { it.toLowerCase() == bit.toLowerCase() }
            ?: fail("Expected enum value but got $bit instead!")
        return enumDescriptor.getElementIndex(name)
    }

    override fun beginStructure(descriptor: SerialDescriptor): CompositeDecoder {
        return when (descriptor.kind) {
            PrimitiveKind.BOOLEAN,
            PrimitiveKind.BYTE,
            PrimitiveKind.CHAR,
            PrimitiveKind.SHORT,
            PrimitiveKind.INT,
            PrimitiveKind.LONG,
            PrimitiveKind.FLOAT,
            PrimitiveKind.DOUBLE,
            PrimitiveKind.STRING,
            SerialKind.ENUM,
            StructureKind.CLASS,
            StructureKind.OBJECT,
            -> this
            StructureKind.LIST -> ArgumentDecoder(bits.slice(index until bits.size))
            else -> fail("Unsupported SerialKind ${descriptor.kind::class.simpleName}")
        }
    }

    override fun endStructure(descriptor: SerialDescriptor) {

    }

    override fun decodeElementIndex(descriptor: SerialDescriptor): Int {
        return if (bits.isEmpty() || index == bits.size) CompositeDecoder.DECODE_DONE else 0
    }

    override fun <T : Any> decodeNullableSerializableValue(deserializer: DeserializationStrategy<T?>): T? {
        return try {
            decodeSerializableValue(deserializer)
        } catch (ex: SerializationException) {
            null
        } catch (ex: IndexOutOfBoundsException) {
            null
        }
    }

    // Gotta override a deprecated method because the internals are using this method for some reason....
    override fun <T : Any> decodeNullableSerializableElement(
        descriptor: SerialDescriptor,
        i: Int,
        deserializer: DeserializationStrategy<T?>
    ): T? {
        return decodeNullableSerializableValue(deserializer)
    }
}
