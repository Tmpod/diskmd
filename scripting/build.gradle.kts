plugins {
    id(Plugins.kapt)
}

dependencies {
    api(core)
}

tasks {
    compileKotlin {
        kotlinOptions {
            jvmTarget = JVM.target
            freeCompilerArgs = listOf(
                CompilerArguments.inlineClasses,
                CompilerArguments.coroutines,
                CompilerArguments.time
            )
        }
    }
}
