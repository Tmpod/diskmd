# disKmd
*"dis-command"*

---
![UNSTABLE!](https://img.shields.io/badge/%E2%9A%A0-UNSTABLE-critical?style=for-the-badge)
> This project is still very new and under heavy development and is nowhere near stability or even usability.
---

[**Discord**](https://discord.com) bot command framework built around [**Kord**](https://gitlab.com/kordlib/kord).

## Getting started

> *To be written*

## License 

This project is licensed under [GNU LGPLv3](https://www.gnu.org/licenses/lgpl-3.0.html). 
You can find [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) at [`COPYING.md`](./COPYING.md) 
and the lesser additions at [`COPYING.LESSER.md`](./COPYING.LESSER.md).
Additionally, all library source files have license headers.

## Contact

You can contact me via Discord direct messages, by sending a friend request to `Tmpod#2525`.
Alternatively, you can join one of the servers I attend, such as [DCA](https://discord.gg/RDKfMcC) or [Kord](https://discord.gg/6jcx5ev).