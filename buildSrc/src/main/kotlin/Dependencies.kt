@file:Suppress("ObjectPropertyName")

object Versions {
    // Main dependencies
    const val kotlin = "1.4.0"
    const val kotlinxSerialization = "1.0.0-RC"
    const val kotlinxCoroutines = "1.3.9"
    const val kotlinLogging = "1.8.3"
    const val slf4j = "1.7.30"
    const val kord = "0.6.2"
    const val koin = "2.1.6"

    const val bintray = "1.8.5"
}

object Repositories {
    const val kotlinx = "https://kotlin.bintray.com/kotlinx"
    const val kord = "https://dl.bintray.com/kordlib/Kord"
    const val gradlePlugins = "https://plugins.gradle.org/m2"
}

object Dependencies {
	// Main dependencies
    const val jdk = "org.jetbrains.kotlin:kotlin-stdlib-jdk8"
    const val `kotlinx-serialization` = "org.jetbrains.kotlinx:kotlinx-serialization-core:${Versions.kotlinxSerialization}"
    const val `kotlinx-coroutines` = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.kotlinxCoroutines}"
    const val `kotlin-logging` = "io.github.microutils:kotlin-logging:${Versions.kotlinLogging}"
    const val `slf4j-simple` = "org.sl4fj:slf4j-simple:${Versions.slf4j}"
    const val `slf4j-api` = "org.sl4fj:slf4j-api:${Versions.slf4j}"
    const val `kord-core` = "com.gitlab.kordlib.kord:kord-core:${Versions.kord}"
    const val `koin-core` = "org.koin:koin-core:${Versions.koin}"
}

object Plugins {
    const val `kotlin-gradle-plugin` = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val `kotlin-serialization` = "org.jetbrains.kotlin:kotlin-serialization:${Versions.kotlin}"
    const val `bintray-gradle-plugin` = "com.jfrog.bintray.gradle:gradle-bintray-plugin:${Versions.bintray}"
    const val kapt = "org.jetbrains.kotlin.kapt"
}
