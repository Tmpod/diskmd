import org.gradle.kotlin.dsl.DependencyHandlerScope
import org.gradle.kotlin.dsl.project

val DependencyHandlerScope.core get() = project(":core")
val DependencyHandlerScope.scripting get() = project(":scripting")

object Library {
    // Repository stuff
    const val repoName = "DisKmd"
    const val repoURL = "https://gitlab.com/Tmpod/disKmd"
    const val description = "Powerful Discord bot command framework."
    const val license = "LGPLv3"

    // Package artifact stuff
    const val name = "diskmd"
    const val userOrg = "tmpod"
    const val group = "dev.$userOrg.$name"
    val version = System.getenv("CI_COMMIT_TAG") ?: System.getenv("CI_COMMIT_SHA") ?: "undefined"
}
