import com.jfrog.bintray.gradle.BintrayExtension
import com.jfrog.bintray.gradle.BintrayPlugin

group = Library.group
version = Library.version

buildscript {
    repositories {
        jcenter()
        maven(Repositories.gradlePlugins)
    }
    dependencies {
        classpath(Plugins.`kotlin-gradle-plugin`)
        classpath(Plugins.`kotlin-serialization`)
        classpath(Plugins.`bintray-gradle-plugin`)
    }
}

plugins {
    kotlin("jvm") version Versions.kotlin
}

repositories {
    mavenCentral()
    mavenLocal()
    jcenter()
}

dependencies {
    api(kotlin("stdlib"))
}

subprojects {
    apply(plugin = "kotlin")
    apply(plugin = "kotlinx-serialization")
    apply(plugin = "com.jfrog.bintray")
    apply(plugin = "maven-publish")

    repositories {
        mavenCentral()
        jcenter()
        maven(Repositories.kotlinx)
        maven(Repositories.kord)
    }

    dependencies {
        api(kotlin("stdlib"))
        api(Dependencies.`kotlinx-serialization`)
        api(Dependencies.`kotlinx-coroutines`)
        implementation(Dependencies.`kotlin-logging`)
        implementation(Dependencies.`kord-core`)
        implementation(Dependencies.`koin-core`)
    }

    tasks {
        compileKotlin {
            kotlinOptions.jvmTarget = JVM.target
        }
    }

    val sourcesJar by tasks.registering(Jar::class) {
        archiveClassifier.set("sources")
        from(sourceSets.main.get().allSource)
    }

    apply<BintrayPlugin>()

    configure<PublishingExtension> {
        publications {
            register(Library.name, MavenPublication::class) {
                from(components["kotlin"])
                groupId = Library.group
                artifactId = "${Library.name}-${project.name}"
                version = Library.version

                artifact(sourcesJar.get())
            }
        }
    }

    configure<BintrayExtension> {
        user = System.getenv("BINTRAY_USER")
        key = System.getenv("BINTRAY_KEY")
        setPublications(Library.name)
        publish = true

        pkg = PackageConfig().apply {
            repo = Library.repoName
            name = Library.repoName
            userOrg = Library.userOrg
            setLicenses(Library.license)
            vcsUrl = Library.repoURL
            websiteUrl = Library.repoURL
            issueTrackerUrl = "${Library.repoURL}/issues"

            version = VersionConfig().apply {
                name = Library.version
                desc = Library.description
                vcsTag = Library.version
            }
        }
    }
}
